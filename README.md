# laravel-user-service

## Getting started

Implement larave 9 and  SQL 

### Feature 
- [ ] CRUDL User
- [ ] User Profile
- [ ] Random Item

### install 

* set up docker 
    ```docker-compose up -d```
* install composer
    ```docker-compose exec app composer install```
* command migrate and seed Database
    ```
    docker-compose exec app php artisan migrate
    docker-compose exec app php artisan migrate:rollback
        
    docker-compose exec app php artisan db:seed
    
    ```


### Postman 
https://api.postman.com/collections/16603329-1bef8955-d0e2-4762-ac70-4cac0a31f2ea?access_key=PMAT-01GW16G95SE1W8K08YGXQPJMXT